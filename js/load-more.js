$(document).ready(function(){
    $(".products-card").slice(0, 9).show();
    $("#load-more").on("click", function(e){
      e.preventDefault();
      $(".products-card:hidden").slice(0, 3).slideDown();
      if($(".products-card:hidden").length == 0) {
        $("#loadMore").text("No Content").addClass("noContent");
      }
    });
  })