// placeholder меняем при смене экрана
window.onload = changePlaceholder;
window.onresize = changePlaceholder;

function changePlaceholder() {
    // js
    if (document.documentElement.clientWidth < 758) {
        document.querySelector("input[type='email']").setAttribute("placeholder", "Subscribe to newsletter");
    } else {
        document.querySelector("input[type='email']").setAttribute("placeholder", "Enter your email address");
    }
}